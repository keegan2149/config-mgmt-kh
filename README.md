# File Index

The following describes the purpose of the files in this folder:

* `ots.vcl` - Our current varnish config installed in `/etc/varnish`
* `init.conf` - Default upstart script template
* `init-football.conf` - Default football api upstart script template
* `varnish` - Goes in `/etc/default`
* `server-setup-tasks.md` - Things that need doing on the server when it's setup.
* `logrotate_node-logs` - New file needed in `/etc/logrotate.d` directory.