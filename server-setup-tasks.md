## Need the following packages installed on the server

* Install varnish version 4.0
* Install nodejs version v0.10.31.
* Install redis-server and redis-cli. I think this is available as a common package.
* Install htop
* Remove apache package
* Install tmux


## Kernel params and network stack tuning.

kernel.pid_max = 2097152
net.nf_conntrack_max = 131072
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.core.rmem_default = 1048576
net.core.wmem_default = 1048576
net.core.netdev_max_backlog = 65536
net.core.somaxconn = 131072
net.core.optmem_max = 25165824
net.ipv4.tcp_rmem = 65536 1048576 16777216
net.ipv4.tcp_wmem = 64436 1048576 16777216
net.ipv4.tcp_max_syn_backlog = 65536
vm.max_map_count = 262144

## Add these entries to `/etc/security/limits.conf`. Reboot is required.

* soft  nofile 1000000
* hard  nofile 1000000

## Create some directories

* create directory `/usr/local/ots` and install these subdirs
    * api-cbk
    * api-football
    * api-mlb
    * api-nba
    * api-nhl
    * api-social
    * api-voting
    * social-admin
* create directory `/var/log/ots` and create these subdirs
    * api-cbk
    * api-nfl
    * api-cfl
    * api-mlb
    * api-nba
    * api-nhl
    * api-social
    * api-voting
    * social-admin

## Change directory permissions

* All subdirectories in `/var/log/ots` need permissions `www-data:root`

## Generate upstart scripts in `/etc/init`

We use upstart to start/stop/restart our nodejs api services and make sure they stay up. We can use the following parameters (APP_NAME, PORT) to auto-generate these upstart scripts from the template `./init.conf`

* api-cbk, 8004
* api-nfl, 8008, api-football, nfl
* api-cfl, 8005, api-football, cfl
* api-mlb, 8006
* api-nba, 8007
* api-nhl, 8009
* api-social, 8002
* api-voting, 8010
* social-admin, 8003

The api-nfl and api-cfl applicatons run from the same api-football code base and we pass an environment variable when the service is started so it knows which sport it's responsible for. The upstart script template for these would follow `./init-football.conf`

## Varnish setup

* Put the `ots.vcl` in the `/etc/varnish` directory
* Change `/etc/default/varnish` so that `VARNISH_VCL_CONF` is `/etc/varnish/ots.vcl`
* Change `DAEMON_OPTS` so that varnish listens on port 80.

## Logrotate

* Add `/etc/logratate.d/node-logs` with the contents from `./logratate_node-logs`.

## OnMetal Server Activation
* Automate the creation of a new VM
* Copy puppet modules onto file system
* Bootstrap with puppet apply
* Test

