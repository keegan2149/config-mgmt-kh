class { "apache":
  absent => true,
}
class {'varnish':
  version => 'latest',
}

class {'varnish::vcl':
  backends => [
    { name => 'default', host => '127.0.0.1', port => '8080' },
    { name => 'api_social', host => '127.0.0.1', port => '8002' },
    { name => 'social_admin', host => '127.0.0.1', port => '8003' },
    { name => 'api_cbk', host => '127.0.0.1', port => '8004' },
    { name => 'api_cfl', host => '127.0.0.1', port => '8005' },
    { name => 'api_mlb', host => '127.0.0.1', port => '8006' },
    { name => 'api_nba', host => '127.0.0.1', port => '8007' },
    { name => 'api_nfl', host => '127.0.0.1', port => '8008' },
    { name => 'api_nhl', host => '127.0.0.1', port => '8009' },
    { name => 'api_voting', host => '127.0.0.1', port => '8010' },
  ],
  template => 'varnish/12c-varnish.vcl.erb',
}

class { 'nodejs':
  version => 'v0.10.31',
}
class { 'redis':
}
class { 'htop':
}
class { 'upstart':
  user_jobs => true,
}
upstart::job { 'api-cbk':
  description    => "Starts and stops api-cbk",
  version        => "1",
  respawn        => true,
  respawn_limit  => '99 5',
  pre_start      => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/ots/api-cbk/sys.log',
  script           => 'script sudo -u www-data sh -c "NODE_ENV=production PORT=8004 /usr/bin/node /usr/local/ots/api-cbk/app >> /var/log/ots/api-cbk/node.log 2>&1',
  post_start     => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Started" >> /var/log/ots/api-cbk/sys.log',
}
upstart::job { 'api-nfl':
  description    => "Starts and stops api-nfl",
  version        => "1",
  respawn        => true,
  respawn_limit  => '99 5',
  pre_start      => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/ots/api-nfl/sys.log',
  script           => 'script sudo -u www-data sh -c "SPORT=nfl NODE_ENV=production PORT=8008 /usr/bin/node /usr/local/ots/api-football/app >> /var/log/ots/api-nfl/node.log 2>&1',
  post_start     => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Started" >> /var/log/ots/api-nfl/sys.log',
}
upstart::job { 'api-cfl':
  description    => "Starts and stops api-cfl",
  version        => "1",
  respawn        => true,
  respawn_limit  => '99 5',
  pre_start      => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/ots/api-cfl/sys.log',
  script           => 'script sudo -u www-data sh -c "SPORT=cfl NODE_ENV=production PORT=8005 /usr/bin/node /usr/local/ots/api-football/app >> /var/log/ots/api-cfl/node.log 2>&1',
  post_start     => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Started" >> /var/log/ots/api-cfl/sys.log',
}
upstart::job { 'api-mlb':
  description    => "Starts and stops api-mlb",
  version        => "1",
  respawn        => true,
  respawn_limit  => '99 5',
  pre_start      => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/ots/api-mlb/sys.log',
  script           => 'script sudo -u www-data sh -c "NODE_ENV=production PORT=8006 /usr/bin/node /usr/local/ots/api-mlb/app >> /var/log/ots/api-mlb/node.log 2>&1',
  post_start     => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Started" >> /var/log/ots/api-mlb/sys.log',
}
upstart::job { 'api-nba':
  description    => "Starts and stops api-nba",
  version        => "1",
  respawn        => true,
  respawn_limit  => '99 5',
  pre_start      => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/ots/api-nba/sys.log',
  script           => 'script sudo -u www-data sh -c "NODE_ENV=production PORT=8007 /usr/bin/node /usr/local/ots/api-nba/app >> /var/log/ots/api-nba/node.log 2>&1',
  post_start     => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Started" >> /var/log/ots/api-nba/sys.log',
}
upstart::job { 'api-nhl':
  description    => "Starts and stops api-nhl",
  version        => "1",
  respawn        => true,
  respawn_limit  => '99 5',
  pre_start      => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/ots/api-nhl/sys.log',
  script           => 'script sudo -u www-data sh -c "NODE_ENV=production PORT=8009 /usr/bin/node /usr/local/ots/api-nhl/app >> /var/log/ots/api-nhl/node.log 2>&1',
  post_start     => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Started" >> /var/log/ots/api-nhl/sys.log',
}
upstart::job { 'api-social':
  description    => "Starts and stops api-social",
  version        => "1",
  respawn        => true,
  respawn_limit  => '99 5',
  pre_start      => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/ots/api-social/sys.log',
  script           => 'script sudo -u www-data sh -c "NODE_ENV=production PORT=8002 /usr/bin/node /usr/local/ots/api-social/app >> /var/log/ots/api-social/node.log 2>&1',
  post_start     => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Started" >> /var/log/ots/api-social/sys.log',
}
upstart::job { 'api-voting':
  description    => "Starts and stops api-voting",
  version        => "1",
  respawn        => true,
  respawn_limit  => '99 5',
  pre_start      => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/ots/api-voting/sys.log',
  script           => 'script sudo -u www-data sh -c "NODE_ENV=production PORT=8010 /usr/bin/node /usr/local/ots/api-voting/app >> /var/log/ots/api-voting/node.log 2>&1',
  post_start     => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Started" >> /var/log/ots/api-voting/sys.log',
}
upstart::job { 'social-admin':
  description    => "Starts and stops social-admin",
  version        => "1",
  respawn        => true,
  respawn_limit  => '99 5',
  pre_start      => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Starting" >> /var/log/ots/social-admin/sys.log',
  script           => 'script sudo -u www-data sh -c "NODE_ENV=production PORT=8003 /usr/bin/node /usr/local/ots/social-admin/app >> /var/log/ots/social-admin/node.log 2>&1',
  post_start     => 'echo "[`date -u +%Y-%m-%dT%T.%3NZ`] (sys) Started" >> /var/log/ots/social-admin/sys.log',
}

sysctl { 'kernel.pid_max': value => '2097152' }
sysctl { 'net.nf_conntrack_max': value => '131072' }
sysctl { 'net.core.rmem_max': value => '16777216' }
sysctl { 'net.core.wmem_max': value => '16777216' }
sysctl { 'net.core.rmem_default': value => '1048576' }
sysctl { 'net.core.wmem_default': value => '1048576' }
sysctl { 'net.core.netdev_max_backlog': value => '65536' }
sysctl { 'net.core.somaxconn': value => '131072' }
sysctl { 'net.core.optmem_max': value => '25165824' }
sysctl { 'net.ipv4.tcp_rmem': value => '65536 1048576 16777216' }
sysctl { 'net.ipv4.tcp_wmem': value => '64436 1048576 16777216' }
sysctl { 'net.ipv4.tcp_max_syn_backlog': value => '65536' }
sysctl { 'vm.max_map_count': value => '262144' }

class { 'ulimit':
  purge => false,
}
ulimit::rule {
  'entry1':
    ulimit_domain => '*',
    ulimit_type   => 'soft',
    ulimit_item   => 'nofile',
    ulimit_value  => '1000000',
}
ulimit::rule {
  'entry2':
    ulimit_domain => '*',
    ulimit_type   => 'hard',
    ulimit_item   => 'nofile',
    ulimit_value  => '1000000',
}
file { '/usr/local/ots':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755';

  '/usr/local/ots/api-cbk':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/usr/local/ots'];

  '/usr/local/ots/api-football':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/usr/local/ots'];

  '/usr/local/ots/api-mlb':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/usr/local/ots'];

  '/usr/local/ots/api-nba':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/usr/local/ots'];

  '/usr/local/ots/api-nhl':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/usr/local/ots'];

  '/usr/local/ots/api-social':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/usr/local/ots'];

  '/usr/local/ots/api-voting':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/usr/local/ots'];

  '/usr/local/ots/social-admin':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/usr/local/ots'];
}

file { '/var/log/ots':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755';

  '/var/log/ots/api-cbk':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/var/log/ots'];

  '/var/log/ots/api-nfl':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/var/log/ots'];

  '/var/log/ots/api-cfl':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/var/log/ots'];

  '/var/log/ots/api-mlb':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/var/log/ots'];

  '/var/log/ots/api-nba':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/var/log/ots'];

  '/var/log/ots/api-nhl':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/var/log/ots'];

  '/var/log/ots/api-social':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/var/log/ots'];

  '/var/log/ots/api-voting':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/var/log/ots'];

  '/var/log/ots/social-admin':
    ensure => directory,
    owner  => 'www-data',
    group  => 'root',
    mode   => '0755',
    require => File['/var/log/ots'];
}

logrotate::rule { 'node_logs':
  path           => '/var/log/ots/api-*/node.log ',
  rotate         => 2,
  rotate_every   => 'daily',
  sharedscripts  => true,
  delaycompress  => true,
  missingok      => true,
  compress       => true,
  postrotate     => ['restart api-cbk','restart api-cfl','restart api-nfl','restart api-mlb','restart api-nba','restart api-nhl','restart api-social','restart api-voting'],
}