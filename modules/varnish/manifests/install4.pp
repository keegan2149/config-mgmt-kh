# == Class: varnish::install
#
# Installs Varnish.
#
# === Parameters
#
# version - passed to puppet type 'package', attribute 'ensure'
#
# === Examples
#
# install Varnish
# class {'varnish::install':}
#
# make sure latest version is always installed
# class {'varnish::install':
#  version => latest,
# }
#

class varnish::install4 (
  $version = latest,
) {

  exec { 'varnish-key':
    command => '/usr/bin/curl https://repo.varnish-cache.org/ubuntu/GPG-key.txt | /usr/bin/apt-key add - && touch /var/log/varnishrepook',
    logoutput => 'true',
    creates => '/var/log/varnishrepook',
    require => Package['curl'],
  }

  exec { 'varnish-repo':
    command => '/bin/echo "deb https://repo.varnish-cache.org/ubuntu/ precise varnish-4.0" >> /etc/apt/sources.list.d/varnish-cache.list',
    logoutput => 'true',
    creates => '/etc/apt/sources.list.d/varnish-cache.list',
  }
  exec { "apt-update":
    command => "/usr/bin/apt-get update && touch /var/log/varnishupdateok",
    logoutput => 'true',
    creates => '/var/log/varnishupdateok',
    onlyif => "/bin/ls /etc/apt/sources.list.d/varnish-cache.list",
  }
  Exec['varnish-key'] -> Exec['varnish-repo'] -> Exec['apt-update'] -> Package['varnish']
  package { 'curl':
    ensure => 'present',
  }
  # varnish package
  package { 'varnish':
    ensure  => $version,
  }
}

